CREATE TABLE categories
(
    id  INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(150)
);
CREATE TABLE posts
(
    id  INT PRIMARY KEY AUTO_INCREMENT,
    category_id INT NULL,
    title VARCHAR(150) NULL,
    description VARCHAR(150) NULL,
    image VARCHAR NULL,
    vote INT default 0,
    create_at timestamp default current_timestamp,
    FOREIGN KEY(category_id) REFERENCES categories(id)
);
CREATE TABLE comments
(
    id  INT PRIMARY KEY AUTO_INCREMENT,
    post_id INT NULL,
    message VARCHAR(150) NULL,
    vote INT default 0,
    FOREIGN KEY(post_id) REFERENCES posts(id)

);
