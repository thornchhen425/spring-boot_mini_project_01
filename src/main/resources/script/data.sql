insert into categories(name)
values ('Spring-boot'),
       ('Java'),
       ('React');
insert into posts(title, description, category_id, vote, image,create_at)
values ('How to config H2 in spring-boot', 'spring-boot', 1, 10, 'spring.jpg','2021-05-31 23:49:18.896119'),
       ('How to use Oauth2', 'spring-boot', 2, 50, 'spring.jpg','2021-05-30 23:49:18.896119'),
       ('How to use Mybatis', 'spring-boot', 3, 30, 'spring.jpg','2021-05-29 23:49:18.896119');


insert into comments(post_id,message, vote) values(1,'Hello guy', 10),(1,'Thank you!!!', 5);