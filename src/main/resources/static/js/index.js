const timeagoInstance = timeago();
timeagoInstance.render(document.querySelectorAll('.time'),'en_EN');

const headers = {
    "Content-Type": "application/json"
}
function postVote(id, inc) {
    const voteEle = document.getElementById(`post-vote-${id}`);
    const vote = parseInt(voteEle.innerText);
    const count = inc ? vote + 1 : vote - 1;

    fetch(`/api/posts/vote/${id}`, {
        method: "post",
        body: JSON.stringify({
            vote: count
        }),
        headers
    }).then((res) => res.json()).then(res => {

        voteEle.innerText = `${count}`;

    }).catch((err) => {
        alert(`Post vote fail`);
    });
}

function commentVote(id, inc) {
    const voteEle = document.getElementById(`comment-vote-${id}`);
    const vote = parseInt(voteEle.innerText);
    const count = inc ? vote + 1 : vote - 1;

    fetch(`/api/comments/vote/${id}`, {
        method: "post",
        body: JSON.stringify({
            vote: count
        }),
        headers
    }).then((res) => res.json()).then(res => {

        voteEle.innerText = `${count}`;

    }).catch((err) => {
        alert(`Comment vote fail`);
    });
}
