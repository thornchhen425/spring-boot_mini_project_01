package com.reddit.clone.springminiproject.service;

import com.reddit.clone.springminiproject.model.Comment;

import java.util.List;

public interface CommentService {
    boolean save(Comment comment);
    boolean update(Comment comment);
    boolean updateVote(Integer vote, Integer id);
    boolean delete(Integer id);
    boolean deleteByPostId(Integer postId);
    Comment getCommentById(Integer id);
    List<Comment> getComments();
    List<Comment> getCommentsByPostId(Integer postId);
}
