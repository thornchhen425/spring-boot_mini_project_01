package com.reddit.clone.springminiproject.service;

import com.reddit.clone.springminiproject.model.Category;

import java.util.List;

public interface CategoryService {
    boolean save(Category category);
    boolean update(Category category);
    boolean delete(Integer id);
    Category getCategory(Integer id);
    List<Category> getCategories();
}
