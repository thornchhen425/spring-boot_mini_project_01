package com.reddit.clone.springminiproject.service.imp;

import com.reddit.clone.springminiproject.model.Comment;
import com.reddit.clone.springminiproject.repository.CommentRepository;
import com.reddit.clone.springminiproject.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImp implements CommentService {
    @Autowired
    private CommentRepository commentRepository;
    @Override
    public boolean save(Comment comment) {
        return commentRepository.save(comment);
    }

    @Override
    public boolean update(Comment comment) {
        return commentRepository.update(comment);
    }

    @Override
    public boolean updateVote(Integer vote, Integer id) {
        return commentRepository.updateVote(vote,id);
    }

    @Override
    public boolean delete(Integer id) {
        return commentRepository.delete(id);
    }

    @Override
    public boolean deleteByPostId(Integer postId) {
       return commentRepository.deleteByPostId(postId);
    }

    @Override
    public Comment getCommentById(Integer id) {
        return commentRepository.getCommentById(id);
    }

    @Override
    public List<Comment> getComments() {
        return commentRepository.getComments();
    }

    @Override
    public List<Comment> getCommentsByPostId(Integer postId) {
        return commentRepository.getCommentsByPostId(postId);
    }
}
