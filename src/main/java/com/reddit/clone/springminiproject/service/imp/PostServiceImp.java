package com.reddit.clone.springminiproject.service.imp;

import com.reddit.clone.springminiproject.model.Post;
import com.reddit.clone.springminiproject.repository.PostRepository;
import com.reddit.clone.springminiproject.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostServiceImp implements PostService {

    @Autowired
    private PostRepository postRepository;

    @Override
    public boolean save(Post post) {
        return postRepository.save(post);
    }

    @Override
    public boolean update(Post post) {
        return postRepository.update(post);
    }

    @Override
    public boolean updateVote(Integer vote, Integer id) {
        return postRepository.updateVote(vote, id);
    }

    @Override
    public boolean delete(Integer id) {
        return postRepository.delete(id);
    }

    @Override
    public List<Post> getPosts() {
        return postRepository.getPosts();
    }

    @Override
    public List<Post> getPostsByCategoryId(Integer categoryId) {
        return postRepository.getPostsByCategoryId(categoryId);
    }

    @Override
    public Post getPostById(Integer id) {
        return postRepository.getPostById(id);
    }
}
