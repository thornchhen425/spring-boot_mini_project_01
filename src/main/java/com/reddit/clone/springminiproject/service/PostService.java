package com.reddit.clone.springminiproject.service;

import com.reddit.clone.springminiproject.model.Post;

import java.util.List;

public interface PostService {
    boolean save(Post post);
    boolean update(Post post);
    boolean updateVote(Integer vote, Integer id);
    boolean delete(Integer id);
    List<Post> getPosts();
    List<Post> getPostsByCategoryId(Integer categoryId);
    Post getPostById(Integer id);

}
