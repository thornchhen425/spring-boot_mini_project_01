package com.reddit.clone.springminiproject.service.imp;

import com.reddit.clone.springminiproject.model.Category;
import com.reddit.clone.springminiproject.repository.CategoryRepository;
import com.reddit.clone.springminiproject.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public boolean save(Category category) {
        return categoryRepository.save(category);
    }

    @Override
    public boolean update(Category category) {
        return categoryRepository.update(category);
    }

    @Override
    public boolean delete(Integer id) {
        return categoryRepository.delete(id);
    }

    @Override
    public Category getCategory(Integer id) {
        return categoryRepository.getCategoryById(id);
    }

    @Override
    public List<Category> getCategories() {
        return categoryRepository.getCategories();
    }
}
