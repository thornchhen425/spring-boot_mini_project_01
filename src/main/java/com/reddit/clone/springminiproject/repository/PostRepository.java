package com.reddit.clone.springminiproject.repository;

import com.reddit.clone.springminiproject.model.Post;
import com.reddit.clone.springminiproject.provider.PostProvider;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface PostRepository {
    @InsertProvider(type = PostProvider.class, method = "addBuilder")
    boolean save(Post post);

    @UpdateProvider(type = PostProvider.class, method = "updateBuilder")
    boolean update(Post post);

    @UpdateProvider(type = PostProvider.class, method = "updateVoteBuilder")
    boolean updateVote(@Param("vote") Integer vote, @Param("id") Integer id);

    @DeleteProvider(type = PostProvider.class, method = "deleteBuilder")
    boolean delete(@Param("id") Integer id);

    @SelectProvider(type = PostProvider.class, method = "selectBuilder")
    @Results(id = "getAll",
            value = {
                    @Result(property = "comments", column = "id", many = @Many(select = "com.reddit.clone.springminiproject" +
                            ".repository" +
                            ".CommentRepository" +
                            ".getCommentsByPostId")),
                    @Result(property = "id", column = "id"),
                    @Result(property = "category", column = "category_id", one = @One(select = "com.reddit.clone.springminiproject" +
                            ".repository" +
                            ".CategoryRepository" +
                            ".getCategoryById"))

            })
    List<Post> getPosts();

    @SelectProvider(type = PostProvider.class, method = "selectByCategoryIdBuilder")
    @ResultMap(value = "getAll")
    List<Post> getPostsByCategoryId(@Param("category_id") Integer categoryId);

    @SelectProvider(type = PostProvider.class, method = "selectByIdBuilder")
    @ResultMap(value = "getAll")
    Post getPostById(@Param("id") Integer id);

}
