package com.reddit.clone.springminiproject.repository;

import com.reddit.clone.springminiproject.model.Category;
import com.reddit.clone.springminiproject.provider.CategoryProvider;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CategoryRepository {

    @InsertProvider(type = CategoryProvider.class, method = "addBuilder")
    boolean save(Category category);

    @UpdateProvider(type = CategoryProvider.class, method = "updateBuilder")
    boolean update(Category category);

    @DeleteProvider(type = CategoryProvider.class, method = "deleteBuilder")
    boolean delete(@Param("id") Integer id);

    @SelectProvider(type = CategoryProvider.class, method = "selectByIdBuilder")
    Category getCategoryById(@Param("id") Integer id);

    @SelectProvider(type = CategoryProvider.class, method = "selectBuilder")
    @Results({
            @Result(property = "posts", column = "id", many = @Many(select = "com.reddit.clone.springminiproject.repository.PostRepository.getPostsByCategoryId")),
            @Result(property = "id", column = "id")
    })
    List<Category> getCategories();
}
