package com.reddit.clone.springminiproject.repository;

import com.reddit.clone.springminiproject.model.Comment;
import com.reddit.clone.springminiproject.provider.CommentProvider;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CommentRepository {
    @InsertProvider(type = CommentProvider.class, method = "addBuilder")
    @Options(useGeneratedKeys = true, keyColumn = "id", keyProperty = "id")
    boolean save(Comment comment);

    @UpdateProvider(type = CommentProvider.class, method = "updateBuilder")
    boolean update(Comment comment);

    @UpdateProvider(type = CommentProvider.class, method = "updateVoteBuilder")
    boolean updateVote(@Param("vote") Integer vote, @Param("id") Integer id);

    @DeleteProvider(type = CommentProvider.class, method = "deleteBuilder")
    boolean delete(@Param("id") Integer id);

    @DeleteProvider(type = CommentProvider.class, method = "deleteByPostIdBuilder")
    boolean deleteByPostId(@Param("post_id") Integer postId);

    @SelectProvider(type = CommentProvider.class, method = "selectByIdBuilder")
    Comment getCommentById(@Param("id") Integer id);

    @SelectProvider(type = CommentProvider.class, method = "selectBuilder")
    @Result(property = "id", column = "id")
    List<Comment> getComments();

    @SelectProvider(type = CommentProvider.class, method = "selectByPostIdBuilder")
    List<Comment> getCommentsByPostId(@Param("post_id") Integer postId);
}
