package com.reddit.clone.springminiproject.provider;

import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {
    public String addBuilder() {
        return new SQL().INSERT_INTO("categories")
                .INTO_COLUMNS("name")
                .INTO_VALUES("#{name}")
                .toString();
    }

    public String deleteBuilder() {
        return new SQL().DELETE_FROM("categories").WHERE("id=#{id}").toString();
    }

    public String updateBuilder() {
        return new SQL().UPDATE("categories")
                .SET("name=#{name}")
                .WHERE("id=#{id}")
                .toString();
    }

    public String selectBuilder() {
        return new SQL().SELECT("*")
                .FROM("categories")
                .toString();
    }

    public String selectByIdBuilder() {
        return new SQL().SELECT("*")
                .FROM("categories")
                .WHERE("id=#{id}")
                .toString();
    }
}
