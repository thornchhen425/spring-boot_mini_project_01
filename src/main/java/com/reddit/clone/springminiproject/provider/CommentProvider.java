package com.reddit.clone.springminiproject.provider;

import org.apache.ibatis.jdbc.SQL;

public class CommentProvider {
    public String addBuilder() {
        return new SQL().INSERT_INTO("comments")
                .INTO_COLUMNS("post_id", "message")
                .INTO_VALUES("#{post_id}", "#{message}")
                .toString();
    }

    public String deleteBuilder() {
        return new SQL().DELETE_FROM("comments").WHERE("id=#{id}").toString();
    }
    public String deleteByPostIdBuilder() {
        return new SQL().DELETE_FROM("comments").WHERE("post_id=#{post_id}").toString();
    }


    public String updateBuilder() {
        return new SQL().UPDATE("comments")
                .SET("message=#{message}")
                .WHERE("id=#{id}")
                .toString();
    }
    public String updateVoteBuilder() {
        return new SQL().UPDATE("comments")
                .SET("vote=#{vote}")
                .WHERE("id=#{id}")
                .toString();
    }

    public String selectBuilder() {
        return new SQL().SELECT("*")
                .FROM("comments")
                .toString();
    }

    public String selectByIdBuilder() {
        return new SQL().SELECT("*")
                .FROM("comments")
                .WHERE("id=#{id}")
                .toString();
    }

    public String selectByPostIdBuilder() {
        return new SQL().SELECT("*")
                .FROM("comments")
                .WHERE("post_id=#{post_id}")
                .toString();
    }

}
