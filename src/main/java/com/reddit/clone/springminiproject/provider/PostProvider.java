package com.reddit.clone.springminiproject.provider;

import org.apache.ibatis.jdbc.SQL;

public class PostProvider {
    public String addBuilder() {
        return new SQL().INSERT_INTO("posts")
                .INTO_COLUMNS("category_id", "title", "description", "image")
                .INTO_VALUES("#{category_id}", "#{title}", "#{description}", "#{image}")
                .toString();
    }

    public String deleteBuilder() {
        return new SQL().DELETE_FROM("posts").WHERE("id=#{id}").toString();
    }

    public String updateBuilder() {
        return new SQL().UPDATE("posts")
                .SET("category_id=#{category_id},title=#{title},description=#{description},image=#{image}")
                .WHERE("id=#{id}")
                .toString();
    }
    public String updateVoteBuilder() {
        return new SQL().UPDATE("posts")
                .SET("vote=#{vote}")
                .WHERE("id=#{id}")
                .toString();
    }
    public String selectBuilder() {
        return new SQL().SELECT("*")
                .FROM("posts")
                .toString();
    }

    public String selectByIdBuilder() {
        return new SQL().SELECT("*")
                .FROM("posts")
                .WHERE("id=#{id}")
                .toString();
    }

    public String selectByCategoryIdBuilder() {
        return new SQL().SELECT("*")
                .FROM("posts")
                .WHERE("category_id=#{category_id}")
                .toString();
    }
}
