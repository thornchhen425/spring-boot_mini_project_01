package com.reddit.clone.springminiproject.model;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Comment {
    private Integer id;
    private Integer post_id;
    private String message;
    private Integer vote;
}
