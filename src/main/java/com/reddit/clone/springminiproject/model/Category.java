package com.reddit.clone.springminiproject.model;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Category {
    private Integer id;
    private String name;
    private List<Post> posts;
}
