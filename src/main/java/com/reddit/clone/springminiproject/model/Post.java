package com.reddit.clone.springminiproject.model;

import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Post {
    private Integer id;
    private Integer category_id;
    private String title;
    private String description;
    private Integer vote;
    private String image;
    private LocalDateTime create_at;
    private List<Comment> comments;
    private Category category;

    public String getImageUrl() {
        return "/images/" + this.image;
    }
}
