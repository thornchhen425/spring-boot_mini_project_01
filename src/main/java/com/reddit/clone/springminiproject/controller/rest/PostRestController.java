package com.reddit.clone.springminiproject.controller.rest;

import com.reddit.clone.springminiproject.model.Post;
import com.reddit.clone.springminiproject.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/posts")
public class PostRestController {

    @Autowired
    private PostService postService;

    @PostMapping("/vote/{id}")
    public boolean vote(@PathVariable() Integer id,@RequestBody Post post){
        return  postService.updateVote(post.getVote(), id);
    }

}
