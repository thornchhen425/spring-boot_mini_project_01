package com.reddit.clone.springminiproject.controller.web;

import com.reddit.clone.springminiproject.model.Post;
import com.reddit.clone.springminiproject.service.CategoryService;
import com.reddit.clone.springminiproject.service.CommentService;
import com.reddit.clone.springminiproject.service.PostService;
import com.reddit.clone.springminiproject.utils.FileUploader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Controller
@RequestMapping("/posts")
public class PostController {
    @Autowired
    private PostService postService;
    @Autowired
    private CommentService commentService;
    @Autowired
    private CategoryService categoryService;

    @GetMapping("/{id}")
    public String post(Model model, @PathVariable("id") Integer id) {
        var post = postService.getPostById(id);
        model.addAttribute("post",post);
        model.addAttribute("page", "view-post");
        return "index";
    }
    @GetMapping("/create")
    public String post(Model model) {
        model.addAttribute("categories", categoryService.getCategories());
        model.addAttribute("post", new Post());
        model.addAttribute("page", "post");

        return "index";
    }

    @PostMapping("/create")
    public String savePost(
            @ModelAttribute Post post,
            @RequestParam("file") MultipartFile file
    ) throws IOException {
        if (!file.isEmpty()) {
            var fileName = FileUploader.upload(file);
            post.setImage(fileName);
        }
        postService.save(post);
        return "redirect:/";
    }

    @GetMapping("/edit/{id}")
    public String edit(Model model, @PathVariable Integer id) {
        var post = postService.getPostById(id);
        model.addAttribute("categories", categoryService.getCategories());
        model.addAttribute("post", post);
        model.addAttribute("page", "post");
        return "index";
    }

    @PostMapping("/edit/{id}")
    public String saveEdit(@ModelAttribute Post post, @PathVariable("id") Integer id,
                           @RequestParam("file") MultipartFile file
    ) throws IOException {
        if (!file.isEmpty()) {
            var fileName = FileUploader.upload(file);
            post.setImage(fileName);
        }
        postService.update(post);
        return "redirect:/";
    }

    @PostMapping("/delete/{id}")
    public String delete(@PathVariable("id") Integer id) {
        commentService.deleteByPostId(id);
        postService.delete(id);
        return "redirect:/";
    }

}
