package com.reddit.clone.springminiproject.controller.web;

import com.reddit.clone.springminiproject.model.Category;
import com.reddit.clone.springminiproject.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/categories")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    @GetMapping("/create")
    public String create(Model model) {
        model.addAttribute("category",new Category());
        model.addAttribute("page", "create-category");
        return "index";
    }
    @PostMapping("/create")
    public String save(@ModelAttribute Category category) {
        categoryService.save(category);
        return "redirect:/";
    }
}
