package com.reddit.clone.springminiproject.controller;

import com.reddit.clone.springminiproject.repository.CategoryRepository;
import com.reddit.clone.springminiproject.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class RootController {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private PostRepository postRepository;

    @GetMapping("/")
    public String home(Model model){
        var categories = categoryRepository.getCategories();
        var posts = postRepository.getPosts();
        model.addAttribute("categories",categories);
        model.addAttribute("posts",posts);
        model.addAttribute("page", "home");
        return "index";
    }






}

