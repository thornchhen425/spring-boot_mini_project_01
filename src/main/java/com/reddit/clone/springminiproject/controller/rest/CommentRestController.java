package com.reddit.clone.springminiproject.controller.rest;

import com.reddit.clone.springminiproject.model.Comment;
import com.reddit.clone.springminiproject.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/comments")
public class CommentRestController {
    @Autowired
    private CommentService commentService;

    @GetMapping
    public List<Comment> getComments() {
        return commentService.getComments();
    }

    @PostMapping(value = "/vote/{id}")
    public boolean vote(@PathVariable Integer id, @RequestBody Comment comment) {
        return commentService.updateVote(comment.getVote(), id);
    }

    @PostMapping(value = "/create")
    public Comment create(@RequestBody Comment comment) {
        commentService.save(comment);
        return comment;
    }
}
